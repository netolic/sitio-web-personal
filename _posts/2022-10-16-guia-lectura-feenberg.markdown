--- 
layout: post
title: "Guía de lectura del texto de Feenberg"
---

>"La tecnología es un fenómeno con dos caras: por un lado el operador, por el otro el objeto. Allí donde el operador y el objeto son seres humanos, la acción técnica es un ejercicio de poder. Más aún: allí donde la sociedad está organizada en torno a la tecnología, el poder tecnológico es la principal forma de poder social, realizado a través de diseños que estrechan el rango de intereses y preocupaciones que pueden ser representados por el funcionamiento normal de la tecnología y las instituciones dependientes de ella. Este estrechamiento deforma la estructura de la experiencia y es causa de sufrimiento humano y de daños al medio ambiente natural."

**¿Qué relaciones podés establecer en esta conceptualización de la tecnología con los desarrollos específicos de software, libre o privativo, con los diseños de las interfaces de usuario (físicas o lógicas) o incluso, frente a la cuestión de la utilidad real del acceso al código fuente para aquellos usuarios/as no programadores?**

En principio, entiendo que el modelo de desarrollo del software libre cuestiona la jerarquía de quienes poseen los conocimientos para el desarrollo técnico en el momento que otorga todas las libertades a les usuaries. Cuando trabajamos creando una pieza de software, tomamos muchísimas decisiones que impactan en cómo las personas se van a apropiar de esa herramienta: desde donde posicionar un botón, cómo está documentado, en qué idiomas lo ofrecemos, etc. En el peor de los casos, esas decisiones son guiadas por la lógica que busca maximizar la rentabilidad de ese producto. Sin embargo, en los casos donde los desarrollos parten de voluntaries u organizaciones que buscan un *bien común* también existe un sesgo producto de las propias limitaciones e incluso de la falta de recursos para poder generar procesos participativos en el desarrollo. Por otro lado, las personas que tienen la posbilidad de realizar esos desarrollos poseen las condiciones materiales y de formación (muchas veces universitaria) para poder hacerlo, lo que no es el caso de muchos otros grupos, que se limitan sólo a ser usuaries de la herramienta.

Es aquí donde las licencias libres dejan abierta la posibilidad de intervenir en ese desarrollo, ya sea directamente en el código o bien participando en las comunidades aportando ideas, documentación e incluso financiamiento para generar las modificaciones deseadas. En el caso del software privativo, esa posibilidad esta vedada desde el comienzo, más allá de poseer o no los conocimientos técnicos necesarios.

---
<br>

> "Un código técnico es la realización de un interés bajo la forma de una solución técnicamente coherente a un problema."

**¿Cuál podría ser un ejemplo del ámbito del software de códigos técnicos que se corresponden con "modos de vida culturalmente asegurados" y ejercicios de poder hegemónicos?**

Pienso en el motor de búsqueda de Google, dado que monopolizó el mercado y se convirtió en la puerta de acceso a internet para gran cantidad de personas, muchas de las cuáles no distinguen la diferencia entre los sitios indexados y el buscador, cristalizado en la frase "lo encontré en google". Debido a que no tenemos ningún tipo de participación en la decisión de como se indexan los sitios web que nos muestra el buscador, estamos a merced de Alphabet y sus vínculos y acuerdos con el *establishment*.

Por otro lado, y a riesgo de irme un poco por las ramas, pienso en las temporalidades que nos proponen las plataformas donde volcamos nuestra actividad creativa. Sabemos que si hacemos un video muy largo no va a ser visto, que una canción tiene que durar no más de tres minutos (¿que pasó con esos discos conceptuales de los '70?) y que si queremos escribir un texto tenemos que captar la atención en el primer párrafo e incluir la metadata necesaria para que sirva al SEO. Últimamente vengo pensando que esos condicionamientos nos quitan la posibilidad de otra temporalidad mas larga, lenta, reflexiva y nos condiciona la creación de piezas artísiticas, que si escapan al formato que proponen esas plataformas corren el riesgo de no ser vistas nunca jamás...

---
<br>

> "Esta descripción ayuda a entender la naturaleza de las controversias éticas que involucran a la tecnología en el mundo real. A menudo, éstas encienden la supuesta oposición entre los estándares corrientes de eficiencia técnica y los valores."

**¿En qué discusiones típicas por el uso/no uso de software libre podría aplicarse esta afirmación? Analicen un caso que conozcan.**

Una discusión de este tipo que indentifico es en relación a los espacios donde transmitimos conocimientos acerca del uso de computadoras[1]. En las experiencias que vengo transitando elijo utilizar software libre, desde el sistema operativo hasta las distintas aplicaciones. En muchos casos, las personas se acercan para adquirir conocimientos relevantes para poder acceder al mercado laboral, y en ese caso la discusión es: "en el trabajo no van a usar linux".

Sería más "eficaz" enseñar a usar la suite de ofimática de Microsoft, o el sistema operativo Windows, y muchas veces esa es la demanda de las personas. Sin embargo, tratamos de enfocar en aprender a usar un sistema y no ser *adiestrado* en el uso del mismo. Si bien lleva más tiempo y compromiso, intentamos generar la capacidad de poder entender una interfaz y poder realizar los procesos que necesitamos. De esta manera, entendemos que la eficacia se logra al empoderar a las personas con herramientas para resolver las tareas independientemente de la *marca* del software que usan, y si por ejemplo, la empresa en la que trabajan tiene problemas con las licencias de un software y deciden cambiar a otro, no va a ser un problema demasiado grande, a fin de cuentas, cualquier software de planilla de cálculos, por ejemplo, permite hacer las operaciones básicas para realizar el trabajo.

[1] [Espacios Abiertos de Tecnología de LTC](https://ltc.camba.coop/2022/06/16/espacios-abiertos-de-tecnologias/)
