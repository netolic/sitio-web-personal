---
layout: post
title: "Jugando a tener infraestructura propia I: raspberry pi + Nextcloud"
---


Internet ha dejado de ser un lugar libre y seguro: la concentración de los servicios de nube, la vigilancia masiva, los rastreadores que buscan crear perfiles de cada usuario y demás *malas yerbas*, nos llevan a la angustiante pregunta de ¡¿Qué hacer?!. 

Es imperante tener la seguridad y confianza que al transmitir archivos, mensajes y datos, sepamos a ciencia cierta quien los recibe, cómo, cuando, donde está alojado (física y virtualmente) y quienes otrxs participan de esa comunicación. En esa transmisión de datos, o *paquetes de información*, va implícita nuestra subjetividad, junto con nuestras opiniones, sentimientos, información personal y *metadatos* (aquellos asociados a los mensajes, que no son parte de su contenido pero permiten trazar un perfil de usuario).

Para tener la seguridad de como se manejan mis datos (o por lo menos algunos de ellos), instalé el sistema operativo **Raspbian** en una **RaspberryPI**, y comencé a montar algunos servicios útiles, entre ellos [Nextlcoud](), un software de *nube* libre, que permite tener archivos en común y sincronizar carpetas. Antes de instalar Nextcloud, me aseguré que el servidor **Apache** montado en la *raspi* sea accesibre de un modo cómodo en la red:

## Dominio .local para la raspi

https://www.howtogeek.com/167190/how-and-why-to-assign-the-.local-domain-to-your-raspberry-pi/

## Instalando Nextcloud en Raspbian 

Si bien existe la posibilidad de instalar [NextCloudPi](https://ownyourbits.com/nextcloudpi/), una instancia de Nexcloud preinstalada y preconfigurada, opté la opción de instalarlo sobre el Raspbian corriendo en la RaspberryPi.

### Prerrequisitos

Nextcloud es una aplicación que para funcionar necesita un *servidor web* para ofrecer acceso a un *cliente* que consuma sus servicios, que además pueda interpretar el lenguaje *PHP* y un *motor de base de datos* para manejar la información de rutas de almacenamiento, sistema de archivos, etiquetas, comentarios, etc. 

Corriendo estos comandos, se instala los requisitos necesarios:
- Apache WebServer
- Motor de base de datos MySQL(MariaDB)
- PHP + modulos.

```
sudo apt install apache2 mariadb-server libapache2-mod-php
sudo apt install php-gd php-json php-mysql php-curl php-mbstring php-intl php-imagick php-xml php-zip
```

### Instalando Nextcloud

Primero obtuve la [última versión de Nextcloud](https://nextcloud.com/install/#instructions-server) desde su sitio web. Luego, extraje el archivo en la ubicación ``` /var/www/html/```, que es el lugar donde Apache *busca* los archivos para ser servidos.

**Nota: La última versión de NextCloud utiliza php7.1, que no está presente en los paquetes de la version ```stretch``` de Raspbian, por se puede optar por una versión anterior, como [nextcloud-15.0.8.zip](https://download.nextcloud.com/server/releases/nextcloud-15.0.8.zip). Por otro lado, se puede actualizar a la versión ```buster``` de raspbian, que incluye entre sus paquetes la versión 7.1 de *php*, como está explicado [acá](###Versión-de-PHP).**	


```
~/Downloads $ sudo unzip nextcloud-16.0.0.zip /var/www/html/
```

Una vez descomprimido el archivo, es necesario otorgarle los *permisos* necesarios para que el servidor Apache pueda operar en él, a través de su usuario ```www-data```. Para hacerlo debemos tener pemisos de *superusuario*, por lo tanto utilizamos el comando ```sudo```.

```
~$ sudo chmod 750 /var/www/html/nextcloud -R #
~$ sudo chown www-data:www-data /var/www/html/nextcloud -R
```
### Configuración de la base de datos

Luego de la instalación del servidor de base de datos MariaDB (que crea un usuario *root*), vamos a crear una base de datos y un usuario especial para nextcloud:

```
sudo mysql
CREATE USER 'nextcloud' IDENTIFIED BY 'añadirPaswword';
CREATE DATABASE nextcloud;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@localhost IDENTIFIED BY ‘añadirPassword’;
FLUSH PRIVILEGES;
quit
```
Con la base de datos creada, podemos acceder via navegador web a nuestra instancia de Nextcloud, para realizar la configuración.

### Configuración de Nextcloud

Al acceder a la URL *midominio.local/nextcloud*, o en su defecto,  a la dirección IP donde se encuentra la raspberry: *192.168.0.XX/nextcloud*, vemos un formulario para completar con distintos datos referentes a la instalación:

#### Creación de una cuenta de administrador

Elegimos Usuario y Contraseña para esta cuenta

#### Configuración de la base de datos

Aquí le indicamos a nextcloud con que datos y a que base se tiene que conectar, completando:

- usuario: nextcloud
- password: password elegido
- Base de datos (Database): nextcloud
- Host: localhost

Si todo sale bien, nos redirecciona a la página de incio de nuestra instancia de NextCloud.

https://raspberrytips.com/install-nextcloud-raspberry-pi/

### Testeando la instalación


Si navegamos hasta la página de configuración (*'settings'*) ```http://midominio.local/nextcloud/index.php/settings/user)```, encontramos la opción *'Overview'* dentro del menu de administración: ```http://midominio.local/nextcloud/index.php/settings/user```, donde podemos ver "Advertencias de seguridad y configuración del sistema". 

#### Módulos faltantes de php

En mi caso, que opté por actualizar la versión de PHP, tuve que instalar algunos módulos 'a mano', utilizando:

```
sudo apt-get install php7.1-modulo_faltante
sudo a2enmod modulo_faltante
sudo service apache2 restart

```
#### Directorio de archivos accesible desde internet

Aparentemente, según la configuración del servidor, la carpeta que contiene los archivos de NextCloud ('nextcloud/data') es accesible desde internet, lo cual representa un problema:

```
“Your data directory and your files are probably accessible from the Internet. The .htaccess file is not working. It is strongly recommended that you configure your web server in a way that the data directory is no longer accessible or you move the data directory outside the web server document root.”
```
Opté por la opción de mover el directorio que contiene los archivos, siguiendo la segunda opción de este post:

https://help.nextcloud.com/t/howto-change-move-data-directory-after-installation/17170

### Certificados ssl auto firmados

https://wiki.debian.org/Self-Signed_Certificate

## Problemas durante la instalación

### Versión de PHP

Para actualizar la versión de PHP, debemos actualizar nuestro sistema operativo, y así poder instalar los paquetes de php7.1 que están disponibles en *buster*.

Editamos el archivo ```/etc/apt/sources.list``` para cambiar la fuente del repositorio de paquetes ```stretch``` por ```buster```:

```
deb http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi
```
Luego, actualizamos el sistema para ver el efecto del cambio de repositorio, y finalmente, actualizamos la versión del sistema operativo:

```
sudo apt-get update && sudo apt-get upgrade
```
Antes de instalar la nueva versión de PHP, vamos a quitar las versiones anteriores con

```
sudo apt-get autoremove '^php.*'
```
Instalamos el nuevo paquete:

```
sudo apt-get install php7.1
```

Para testear si todo salió bien, podemos crear una archivo que contenga la función ```<?php phpinfo(); ?>``` y colocarlo en el *root* de nuestro servidor:
```
touch /var/www/project1/html/phpinfo.php && echo '<?php phpinfo(); ?>'
```

http://www.heidislab.com/tutorials/installing-php-7-1-on-raspbian-stretch-raspberry-pi-zero-w



## Problemas al intentar cambiar la ruta a el directorio Data

Agregué un pendrive a la raspy. Seguí este hilo:

https://help.nextcloud.com/t/howto-change-move-data-directory-after-installation/17170

durante el proceso obtuve un error de PHP, 

´´´
PHP Fatal error:  Uncaught OCP\AppFramework\QueryException: Could not resolve defaultTokenProvider! Class defaultTokenProvider does not exist in /var/www/html/nextcloud/lib/private/AppFramework/Utility/SimpleContainer.php:110
Stack trace:
´´´
https://help.nextcloud.com/t/class-defaulttokenprovider-does-not-exist/36692/7

no recordaba usuario y contraseña de mysql, entonces:

