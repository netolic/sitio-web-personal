---
layout: post
title:  "Editando Wikipedia y Wikiversidad"
---

A raíz de la propuesta de la docente de Procesos Educativos y Software Libre de la TUSL, comencé la tarea de identificar alguna temática que me interese en Wikipedia o Wikidiversidad e intentar hacer algún aporte significativo a algún artículo. Para eso, antes tuve que explorar cómo funciona la edición en Wikipedia.

## ¿Qué hay detrás de un artículo?

Cuando vemos un artículo de Wikipedia, además del contenido del tema en cuestión, podemos encontrar información sobre quién o quienes lo escribieron, el historial de cambios, discusiones que se dieron sobre este artículo, y por supuesto, la posibilidad de editarlo.

Siguiendo la consigna, me creé un usuario en Wikipedia, y comencé a explorar artículos. La sección de discusión es muy interesante, un lugar donde los usuarios debaten sobre los cambios, revisiones y el contenido en general. Se busca que no sea un foro de *opiniones*, sino que se centre en la edición y en la no-neutralidad del artículo.

La otra sección muy interesante es el Historal, donde se puede ver cada aporte, reversiones y diferencias entre versiones. Además de los usuarios que se quieren hacer los *graciosos* poniendo cualquier cosa en el articulo, me llamó la atención la cantidad de usuarios y *bots* de Wikipedia que realizan tareas de revertir ediciones *vandálicas*.

## Mi primera edición

Buscando algún tema para aportarle contenido, di con el artículo de [Ciudadanía Digital](https://es.wikipedia.org/wiki/Ciudadan%C3%ADa_digital). El artículo menciona a Jordi Adell i Segura, un profesor de pedagogía valenciano, pero en el link que vinculaba su nombre con su biografía, algún *gracioso* cambió el link que apuntaba a ese recurso. Entoncés, utilicé el editor visual para modificar el enlace. 

Me quedé con la duda de quién y cuando se había hecho el cambio en ese link, así que buceando en el historial, con la herramienta visual que te invita a explorarlo interactivamente, encontré en [esta contribución](https://es.wikipedia.org/w/index.php?diff=118835417&oldid=118735596&title=Ciudadan%C3%ADa_digital) el cambio realizado, hace poquitos días, el 02-09-2019.

## Segundo aporte

Leyendo el articulo sobre Ciudadanía Digital, llegué al concepto de *Alfabetización Digital*, y recordé que en algún momento había conocido un proyecto muy interesante de la Fundación Mozilla: [Web Literacy](https://learning.mozilla.org/en-US/web-literacy/read/search/).

Este proyecto esta orientado a analizar y definir las habilidades necesarias utilizar internet de manera responsable, conciente y creativa. Tambíen provee herramientas y lineamientos para trabajar en la transmisión de estos conocimientos.

Entonces, aporté en la sección "Del acceso a la comprenión crítica" un resumen del ["Whitepaper"](https://mozilla.github.io/content/web-lit-whitepaper/) del proyecto de Mozilla, Esta vez usé el editor de código.

Este es el artículo: https://es.wikipedia.org/w/index.php?title=Alfabetismo_digital&oldid=119999438

Dejé un punto de polémica, al usar lenguaje inclusivo... veremos que pasa!
