---
layout: post
title: "Generador de sitios estáticos Jekyll + Gitlab CI"
---

# Gitlab Pages

https://gitlab.com/help/user/project/pages/index.md

# Gitlab CI

https://gitlab.com/help/ci/README.md

# Problemas

## Corriendo bundler

Corri el comando 'exec bundle jekyll serve' y obtuve este error:

```
Traceback (most recent call last):
	2: from /usr/local/bin/bundle:23:in `<main>'
	1: from /usr/lib/ruby/2.5.0/rubygems.rb:303:in `activate_bin_path'
/usr/lib/ruby/2.5.0/rubygems.rb:284:in `find_spec_for_exe': Could not find 'bundler' (1.13.6) required by your /home/neto/proyectos/sitio-web-personal/Gemfile.lock. (Gem::GemNotFoundException)
To update to the latest version installed on your system, run `bundle update --bundler`.
To install the missing version, run `gem install bundler:1.13.6`
```

Corrí 'bundle update --bundler' y eso modificó el archivo Gemfile.lock, cambiando las versiones de ruby y bundler. Cuando pushié los cambios a gitlab fallo la integración, y mostró un error de 'bundle install You must use Bundler 2 or greater with this lockfile.'. Reverti los cambios en la Gemfile y pude avanzar en la integracion.



