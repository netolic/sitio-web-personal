---
layout: post
title: "Recorrido"
---

El primer recuerdo que tengo de intentar aprender algo "por las mías", es a los 15 años, cuando con un cancionero de León Gieco, comencé a intentar tocar la guitarra, y lograr lo que parecia casi imposible: tocar y cantar al mismo tiempo. Tiempo después, comencé a tomar clases y rápidamente hice amistad con varixs músicxs mas experimentadxs, y comencé a participar de las "tocatas". Ahí fue mucho de aprender mirando, escuchando, que te pasen algúna partitura o cifrado, y sobre todo compartiendo el momento y la onda. Durante varios años siguientes me formé en guitarra y percusión con profes y a medida que adquiría herramientas, con videos y partituras. También yendo a ver muchos grupos.

Después me comencé a interesar por el circo de calle, y comencé a aprender malabares y acrobacia de manera *informal*, en un dinámica parecida a los primeros momentos en la música. Al tiempo, hice más regular el entrenamiento con tallere y seminarios, y comencé a trabajar con un grupo, con gente más experimentada. Esa instancia de producir y aprender al mismo tiempo es por un lado bastante frénetica, y por otro te hace avanzar muchísimo. Luego de  un par de años de estar trabajando en el formato de circo de calle, comencé una carrera formal de teatro, en el Instituto Municipal de Avellaneda.

Fueron 4 años de cursada, de lunes a viernes, donde además de Actuación, la materia *troncal* de la carrera, cursé otras como Técnica del Movimiento, Teatro de Objetos, Plástica, Escenografía, etc. El último año está orientado a la producción, y por el enfoque de la institución, busca formar en la producción independiente, repitiendo otra vez esa instancia de poducción y aprendizaje al mismo tiempo.

En el transcurso, como ya venía haciendo algunos trabajos de diseño gráfico para clientes del barrio, empecé a formarme a través de foros de internet en el desarrollo de sitios web. En ese momento, conocí un *hacker* que quería aprender a hacer malabares, Josx, y comenzamos a intercambiar conocimientos de malabares y programación. Además, conocí el software libre, y empecé un camino de aprendizaje y concientización sobre el uso de la tecnología.

Tiempo después, con Josx, comenzamos a armar Crear, una cooperativa de desarrollo de software, que luego derivaría en Cambá, la coope donde trabajo ahora. Ahí comenzó otra etapa de aprendizaje, por un lado, conocimientos técnicos, y por otro aprender como se hace una empresa cooperativa, conocimiento que, *a priori*, no teníamos.

La cosa avanzó y el proyecto se estabilizó y camina. Una parte importante de ese (este) recorrido, fue el desarrollo de actividades educativas. Primero desde la intuición, con mucho recorrido en shows, animaciones y talleres de malabares, y luego buscando formalizarlo, sumando una socia artista visual y docente a la cooperativa, Belén, para dar cauce a nuestros conocimientos técnicos. Hay una alegría en el compartir conocimiento, y un momento de juego y entusiasmo en las instancias creativas, que es lo que principalemente buscamos transmitir. Eso de aprender en grupo, y que lo que hacemos sea la excusa de entretejer vinculos entre nosotrxs, es lo que nos parece más importante. 

Sin embargo me di cuenta que está bueno complementar ese *espiritu* de como hacer las cosas, con conocimientos más sólidos: no es lo mismo mostrar algo que enseñar a hacerlo. Ahí es donde, buscando carreras relacionadas a la educación y la tecnología, dí con esta tecnicatura y comencé el viaje de *ponerme a estudiar posta*, ya que la escuela de teatro era más práctica que teórica. El balance que hago hasta ahora es muy positivo, tuve que *bajar* conocimientos que tengo desperdigados, ordenar la información, citar autores, etc., y de alguna manera *forzandome* a sistematizar conocimientos, algo que creo que suma mucho a la hora de intentar transmitirlos. 

   




