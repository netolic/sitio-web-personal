---
layout: post
title: "Modelos Pedagógicos y SL"
---

> A partir del artículo "El software libre en educación" y el artículo comparativo de los modelos pedagógicos que es posible asociar al software libre y al software privativo, vamos a pensar en un ejemplo más para añadir en uno de los aspectos comparados, y las actividades que permite y las que no, las actitudes que estimula y su valor social, y otros aspectos que consideren pertinentes.

Si bien los artículos y materiales de lectura del módulo cubren un amplio espectro de las diferencias y ventajas/desventajas en relación al software libre y privativo, podemos agregar un aspecto que relacionado a la vinculación entre las personas, y para ello pongo en juego un poco lo que experimenté en lo personal cuando comencé el acercamiento al software libre.

La característica principal  de este acercamiento fue el vínculo directo con "personas humanas" y de manera presencial. Algo que define al modelo de desarrollo de software libre es el sentido de comunidad y trabajo colaborativo. En general (aunque no en todos los casos) esas comunidades están compuestas por personas apasionadas en su actividad y deseosas de compartir, contar y explicar acerca de su trabajo. Entonces se genera un vínculo de acompañamiento que potencia tanto el descubrimiento de aspectos técnicos como el humano. Un ejemplo de esto son los eventos de software libre, como los FLISOL, los LUGs, y otros (recuerdo con mucho cariño a la comunidad misionera de MISOL - Misiones Software Libre y los eventos que organizaban). Ir a un evento te permite conocer personas, que te comentan de otros eventos, y así...

En el caso del software privativo, detrás de desarrollo en general hay empresas, y muchas veces las personas que desarrollan ese software no tienen un vinculo con el producto que realizan. Por otro lado las licencias restrictivas no solo que no fomentan sino que impiden generar un vínculo interesante con les usuaries.

Entonces, utilizar software libre como heramienta en la educación nos permite varias cosas:
- develar el origen de la herramienta que usamos: detrás de un proyecto de SL hay una comunidad de personas, y en general, están predispuestas al intercambio de conocimiento y al vínculo social
- generar un sentido de "consumo responsable", teniendo la posibilidad de participar de esos proyectos como usuarie ya sea reportando fallos, sugiriendo mejoras o ayudando a concretar estrategias de financiamiento.
- empoderarse en el uso de la herramienta, tanto en los foros o ambientes de intercambio de conocimiento como en el acceso al código fuente.


