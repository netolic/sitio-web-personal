--- 
layout: post
title: "Editando Wikipedia 2022 - ¿seguirán ahi mis contribuciones anteriores?"
---


# Editando Wikipedia 2022 - ¿seguirán ahi mis contribuciones anteriores?

En esta nueva edición de la cursada de la materia, lo primero que hice fue ir a ver que pasó con mis contribuciones anteriores. Para ello accedí a mi cuenta en wikipedia e ingresé en la pestaña "Contribuciones". Ahí pude ver las contribuciones filtradas por mi nombre de usuario y constatar que siguen ahí.

Luego me puse a revisar la [contribución hecha](https://es.wikipedia.org/w/index.php?title=Alfabetismo_digital&diff=119999438&oldid=119851034) al articulo de "Alfabetización Digital", y ver si tuvo modificaciones posteriores. Para ello hice click en "Ir a la siguiente Diferencia" y pude ver en [esta contribución](https://es.wikipedia.org/w/index.php?title=Alfabetismo_digital&diff=next&oldid=119999438) que un usuarie no identificado (solo veo la IP) hizo algunas correciones menores en la redacción.

Con respecto a las preguntas disparadoras de la actividad:

- Al menos en la experiencia de estas contribuciones, hubo una buena recepción e incluso una mejora a lo que se escribió.
- Creo que entre las dificultades que le pueden surgir a les estudiantes, está una que me surgió a mí: ¿que tengo yo para aportar en wikipedia?¿de que puedo hablar con seguridad?¿sé lo suficiente como para aportar algo?. Por otro lado, el requerimiento de citar las fuentes de lo que uno escribe, implica mucho trabajo de lectura previo, asi como de sistematización de los conocimientos.
- Creo que un enfoque para resolver esas dificultades puede ser trabajar un tema profundamente, buscando bibliografía, leyendo opiniones diversas o contrarias, y luego, como paso final realizar la edición.
