---
layout: post
title: "Herramientas imaginarias"
---

## La cápsula del tiempo

Uno de los factores más influyentes en el aprendizaje de algo es el tiempo que le podemos dedicar. Muchas veces me encuentro pensando "si tuviera tiempo me gustaría aprender tal cosa", o "me gustaría tener más tiempo para profundizar en esto". A veces ese tiempo puede ser usado para leer mucho acerca de algo, y poder tomar momentos para reflexionar. Otras veces, cuando se quiere adquirir alguna técnica, el tiempo es usado en la repetición de la actividad hasta que podamos "internalizarla". En todos los casos, el estudio o práctica de algo deja una huella en nuestro cuerpo que está ligada inevitablemente al tiempo dedicado.

Por eso se me ocurre pensar en una cápsula dentro de la cual el tiempo no transcurra, o lo haga muchísimo más lento. Por ejemplo, si quiero dedicar dos horas a practicar un ejercicio de guitarra, puedo entrar a las cinco de la tarde, estar las dos horas practicando, y salir... ¡a las 5 de la tarde!

En el corto plazo, imagino que podríamos acelerar (en el tiempo fuera de la capsula) los procesos de aprendizaje y llegar "mas tranqui" con las fechas de entrega.
En el largo plazo... ahora que lo pienso veo que puede haber algunos problemas, por ejemplo, si uso mucho la cápsula estaría envejeciendo un poco más rápido que las otras personas.. es decir, acumularía experiencia pero el paso del tiempo sería visible en mi cuerpo. Siguiendo el juego puede llegar a haber una generación de jóvenes-viejos super sabios.

Con respecto al impacto en el sistema educativo, veo que no sería muy útil en las instancias iniciales de formación, ya que entiendo que es muy importante el tiempo compartido con otres, y los procesos en conjunto. En cambio, podría ser útil en las instancias superiores, donde la formación se realiza en conjunto de las obligaciones laborales o familiares. 
