---
layout: post 
title: "Trabajo Final Procesos Educativos y Software Libre"
--- 
# Paso 1


## Datos de la institución

-   Escuela Adolfo Kapeluz, Las Chacras, Córdoba
-   Entrevistado: Maestro en Literatura y TIC&rsquo;s en jornada extendida para 4°, 5° y 6° Grado.


## Relevamiento sobre el uso de computadoras

-   Utilizan las notebooks de Conectar Igualdad
-   El sistema operativo que utilizan es Windows 8.
-   Utilizan OpenOffice como procesador de textos en entorno windows.
-   Les chiques entraban a Huayra para utilizar las aplicaciones de Juegos.
-   Les docentes no utilizan Huayra porque no estaba configurada la conexión a internet.
-   También comentan que en Windows, el sistema se realentizaba por el bloqueo de Conectar Igualdad y no cuentan con alguien que lo pueda resolver.



## Dificultades para la utilización de SL en el aula

Según el entrevistado, la mayor dificultad esta relacionada con la capacitación a les docentes para permitir ir &ldquo;soltando miedos&rdquo; y ganar en seguridad en el uso de estas herramientas



## Infraestructura

La escuela cuenta con dos gabinetes con computadoras completas, hay que actualizar el software del 90% de ellas. Ese es el desafío que tienen para este año.



# Paso 2

https://nubecita.camba.coop/s/yM87MMxRAsfEBZ8

# Paso 3

Usan las computadoras luego de realizar actividades de creación literaria y exploración de libros. Para una de las actividades utilizan el software [Historias para armar](https://www.historiasparaarmar.org/), que cuenta con personajes y escenarios prediseñados y una dinámica &ldquo;paso a paso&rdquo; para crear la historia. Luego utilizan el procesador de textos OpenOffice for Kids, que tiene menos herramientas que la versión para adultos.

El trabajo se realiza de manera individual y presencial. El docente proyecta su pantalla para indicar donde están las herramientas a utilizar.



# Paso 4



## Propuesta de actividad



### Radio Teatro

La actividad consiste en la escritura y producción de un radio teatro de manera grupal.

-   Se elige una historia a partir de las actividades de creación literaria y exploración de libros.
-   Se realiza la escritura colaborativa de un guión utilizando [Etherpad](https://code.etherpad.com/) (ideal si la instancia está instlalada en un servidor local en la escuela)
    -   El guión debe contemplar los siguientes elementos:
        -   Voz narradora
        -   Diálogos de personajes
        -   Musica ambiental / efectos de sonido originales o reutilizables ( en este caso utilizar materiales con licencias que lo permitan )
-   Grabación y edición de la pieza utilizando [Audacity](https://www.audacityteam.or), presente en Huayra Linux
-   Opcional: creación de versiones gráficas de los personajes utilizando la aplicación [Avatar](https://huayra.educar.gob.ar/tutorial-huayra-avatar/), también presente en Huayra Linux.



## Estrategias



### ¿Cómo responder a las principales objeciones, resistencias o imaginarios adversos al SL presentes en los destinatarios de la propuesta (según relevamiento inicial)?

Según el relevamiento inicial, no hay objeciones eticas o prejuicios en relación al uso de software libre, sino que es la falta de conocimiento el mayor desafío. En ese sentido, y teniendo en cuenta la experiencia relatada en la actividad anterior, para realizar una actividad de este tipo habría que diseñar una estrategia de capacitación en por lo menos dos planos:

1.  Capacitación general en el uso de SL/Huayra Linux

    En respuesta a la sensación de inseguridad frente a la computadora, esta capacitación brindaría los conceptos generales de software, sistema operativo, sistema de archivo y entorno de escritorio (incluyendo configuraciones de sonido, red, medios extraíbles, etc.)

2.  Capacitación específica

    Esta capacitación está orientada al uso de las herramientas específicas. Debería brindar las nociones básicas para utilizar Audacity, Etherpad y Avatar.



### ¿Cómo aportar valor a la propuesta con características diferenciales del software libre elegido, que pueden ser técnicas, legales o éticas, o una combinación de ellas

En primer lugar, el software elegido está presente en Huayra Linux, ya instalado en las computadoras que utilizan. Por otro lado, Audacity y Etherpad a través del navegador son multi-pltaforma, con lo cual alguien puede experimentar en otras computadoras ya que sus licencias lo permiten.

Hay dos aspectos que también agregan valor a la propuesta, uno de ellos es la re-utilización de contenido de audio con licencias libres, esto da la oportunidad de charlar (principalmente en la capacitación docente) acerca de la propiedad intelectual de los contenidos que usamos y producimos.

El otro aspecto interesante se da en el proceso de grabación del guión, donde les alumnes son les que pondrán sus voces en juego, luego escucharlas, modificarlas, etc. Aquí será fundamental un ambiente ameno, donde haya lugar para todes les que quieren participar y les que no.



### ¿Cómo superar las dificultades objetivas que se presentarán en el uso del nuevo software, con qué argumentos se trabajarán las soluciones o alternativas?

Esta actividad tiene un grado de complejidad grande ya que presenta varios desafíos técnicos y de dinámicas de clase. Para los primeros puede ser útil contar con material impreso o digital con tutoriales paso a paso para la utilización de los programas, sobre todo en la edición de audio.

Una estrategia puede ser contar con apoyo externo en la etapa de edición, en este caso podría ser articulando con la radio comunitaria FM Las Chacras 104.9, que se encuentra a unas cuadras y donde utilizan Audacity para la edición radial. Incluso la grabación puede ser en el estudio con el que cuenta la radio.

Si bien es una actividad extensa, tiene productos en cada etapa:

-   Elección de una historia luego de la exploración
-   Confección del guión
-   Grabación
-   Edición

Esto permite acotar la actividad si es necesario y llegar sin embargo a un producto realizado.

Por último, para trabajar las dificultades que surjan se propone enmarcarse en la filosofía del software libre, donde la construcción del conocimiento es colaborativa y circula libremente. Esto implica también la capacidad de asumir lo que no se sabe y poder buscarlo en las fuentes confiables ya sea en el territorio o internet. También es muy importante darle valor a lo producido, muchas veces nos frustramos porque creemos que puede &ldquo;salirnos mejor&rdquo;, sobre todo en las creaciones artísticas. Aquí entonces es importante no descartar los emergentes, y poner en valor lo que se genera.



## Conclusiones

Esta actividad implica un involucramiento de varios actores en la escuela y la comunidad, sobre todo para la capacitación de les docentes, que debería ser abordada a lo largo del año, cuidando de no recargar el trabajo diario.
A pesar de la complejidad y los saberes específicos que se ponen en juego, dejaría como resultado también capacidades instaladas para mejorar el uso de TIC&rsquo;s en el aula.

