--- 
layout: post
title: "El Barrio Digital"
---

# El Barrio Digital

Vivimos en una época donde se expandieron las posibilidades de vinculación y comunicación entre las personas: Internet y los dispositivos electrónicos como las computadoras o los teléfonos ****inteligentes**** nos permiten organizar y acceder a información como nunca antes en la historia de la humanidad, así como intercambiar saberes, archivos multimedia, noticias e incluso construir vínculos significativos con personas de manera ****virtual****.

El conjunto de dispositivos, aplicaciones y redes que utilizamos configuran un ****espacio virtual**** en el que nos ****movemos**** día a día, y cada vez cobra más relevancia en nuestra cotidianeidad. Hoy es común decirle a alguien &ldquo;te ví en facebook&rdquo; o &ldquo;nos juntamos el viernes&#x2026;en una videollamada&rdquo;, es decir que ****habitamos**** ese ****lugar**** al mismo tiempo que habitamos los espacios presenciales.

Sin embargo, muchas veces no tenemos en cuenta los cuidados necesarios que tenemos que tomar, así como lo hacemos cuando nos movemos en los espacios públicos, en el &ldquo;Barrio Digital&rdquo;. Aquí compartiremos una serie de recomendaciones básicas para propiciar un buen caminar por el barrio, disfrutando las posibilidades que nos brinda tomando los recaudos necesarios.



## Los Datos Personales

Los datos personales son toda la información que se relaciona con una persona, incluso aquella que pueda identificarte indirectamente. Por ejemplo DNI, CUIT, fotos, videos, datos de localización, etc.

En Argentina, la [Ley 25326](<https://www.argentina.gob.ar/justicia/derechofacil/leysimple/datos-personales>) garantiza que puedas tener control sobre quien utiliza esos datos, para qué, donde se guardan y hasta cuando, etc.

Es importante tener en cuenta que cuando utilizamos aplicaciones provistas por empresas privadas (por ejemplo Facebook, Instagram, Youtube, Juegos, etc) brindamos nuestros datos personales, que luego son utilizados para diversos fines: desde mejora de los productos hasta la venta de esos datos (incluyendo tiempos de uso, preferencias, interacciones, etc) para la venta de publicidad por parte de terceros.

Nuestra Ley nos da derecho a acceder a nuestros datos personales para rectificarlos, modificarlos o actualizarlos o eliminarlos.

Fuente: <https://www.argentina.gob.ar/justicia/convosenlaweb/situaciones/como-proteger-mis-datos-personales>



## Identidad Digital / Reputación en Línea

Todas nuestras acciones dejan una &ldquo;huella digital&rdquo;, que está compuesta por datos públicos (CUIT/CUIL, cargos, resoluciones judiciales, etc), datos publicados por otres (fotos, posteos de amigos o instituciones,etc.) y datos que generamos nosotres, como las publicaciones que hacemos en las redes.

De esta manera construimos nuestra &ldquo;Reputación en Línea&rdquo;, que incluye nuestras expresiones personales, acciones de terceros (donde nos citan, arroban o generan un hashtag que nos menciona) y también por omisión, ya que no tener cuentas en redes sociales es de por sí un dato que se incluye en nuestra reputación.



### ¿Por qué es importante?

La reputación en línea es una forma de presentación personal: cuando presentamos un CV o nos presentamos a una convocatoria se buscarán referencias en internet. También, es nuestro &ldquo;capital social&rdquo; y representa una parte de quiénes somos.



### Para tener en cuenta

Para cuidar nuestra reputación online debemos tener en cuenta configurar la privacidad de nuestras redes sociales así como los permisos de lectura de las publicaciones.

Por otro lado, para cuidar la reputación online de otres, es importante no publicar nada sin tener su autorización.

Fuentes:

-   <https://www.argentina.gob.ar/justicia/convosenlaweb/situaciones/como-puedo-cuidar-mi-reputacion-en-linea-con-google-alerts>
-   <https://www.argentina.gob.ar/justicia/convosenlaweb/situaciones/que-es-la-huella-digital-en-internet>


## Redes Sociales Libres

Las empresas que nos ofrecen las redes sociales más populares tienen un modelo de negocios basado en la recopilación de datos a gran escala de sus usuaries, para luego ser procesados y vendidos a terceros, de manera que se pueda dirigir publicidad específica relacionada con los gustos y comportamientos de los distintos perfiles. El gran problema con este modelo de negocios es que implica la necesidad de hacer que les usuaries pasen más tiempo utilizando estas aplicaciones, para así poder registrar mas datos acerca de preferencias, comportamientos, etc. Es por esta razón que estas aplicaciones resultan adictivas por diseño.

Por otro lado, cuando aceptamos los términos y condiciones de estos servicios, no solo acordamos en otorgarles nuestra información personal, sino que las jurisdicciones legales que aplican a esas empresas no se encuentran en nuestro pais.



### ¡Bienvenides al Fediverso!

No podemos negar los beneficios que nos otorgan las tecnologías de la información y comunicación, y a pesar de que las redes sociales más conocidas están basadas en modelos de negocio nocivos para les usuaries, la posibilidad de comunicarnos por medios digitales seguros es una necesidad cada vez más fuerte.

Gracias al software libre y al trabajo de muchísimas personas y organizaciones, existen una serie de aplicaciones &ldquo;libres&rdquo; nucleadas en el &ldquo;[Fediverso](<https://joinfediverse.wiki/What_is_the_Fediverse%3F/es>)&rdquo;. Algunas de las ventajas del fediverso son:

-   Aplicaciones descentralizadas: una institución puede tener su red social funcionando en su propio servidor, o bien en el servidor de una organización en quien confía.
-   Distintas aplicaciones para distintos usos, con una sola cuenta para ingresar a todas ellas.
-   Las aplicaciones no son adictivas por diseño, al no tener un modelo de negocios basado en la extracción de datos.
-   Al ser descentralizadas, de software libre y sin basarse en la extracción de datos, las aplicaciones son mas seguras y controlables por sus usuaries.
-   No dependen de coroporaciones, sino de organizaciones sociales y grupos de desarrolladorxs que gestionan democráticamente el mantenimiento de las aplicaciones.



## Versión PDF ilustrada

-   Imagenes fuente:
    -   <https://freesvg.org/vector-drawing-of-neighbourhood-with-a-park>
    -   <https://freesvg.org/1526868577>
-   [Formato PDF](<https://nubecita.camba.coop/s/7gH6KdgWRYJFskZ>)
-   [Formato SVG](<https://nubecita.camba.coop/s/PnH5Hmjtr8y4Wem>)

