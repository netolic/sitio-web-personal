---
layout: page
title: Acerca de mi 
permalink: /about/
---

Apasionado de la actividad creativa en diversas disciplinas, guitarrista de fogón desde joven, pichón de acordionista cumbiero de grande. Clown, malabarista y entusiasta de las tecnologías que nos hacen libres y su potencial transformador del mundo en un lugar más justo (texto copypasteado de la web de nuestra empresa :p ). Trabajo en [camba.coop](https://camba.coop), una cooperativa de trabajo dedicada al desarrollo de software, y desde 2016 coordino el departamento educativo, donde trabajamos en torno a herramientas de software y hardware libres [https://ltc.camba.coop](https://ltc.camba.coop)
